//#![feature(const_fn)]

//#![feature(duration_from_micros)]

#[macro_use]
extern crate lazy_static;

extern crate serial;

use std::env;
use std::io;
use std::time::Duration;

use std::ffi::OsString;
use std::os::unix::ffi::OsStringExt;

//use std::io::prelude::*;
use serial::prelude::*;


fn main() {
    for arg in env::args_os().skip(1) {
        //let bytes = OsString::into_vec(arg);
        //println!("crc8 {}", crc8(&bytes));
        let mut port = serial::open(&arg).unwrap();
        interact(&mut port).unwrap();
    }
}

const EMOLOG_PROTOCOL_VERSION : u16 = 1; // defined in emolog_protocol.h

const WIDTH: usize = 8;
const POLYNOMIAL: u8 = 0xD8; // 11011 followed by 0's
const TOPBIT: u8 = 1 << (WIDTH - 1);

// TODO: const fn didn't work, too many errors
//const crc_table : [u8 ;256] = crc_init();

lazy_static! {
    static ref CRC_TABLE: [u8; 256] = crc_init();
}

fn crc_init() -> [u8 ;256]
{
    let mut table : [u8 ;256] = [0u8 ;256];
    for dividend in 0u16..256u16 {
        let mut remainder = (dividend as u8) << (WIDTH - 8);
        let mut bit = 8u8;
        while bit > 0 {
            remainder = if remainder & TOPBIT > 0 {
                (remainder << 1) ^ POLYNOMIAL
            } else {
                remainder << 1
            };
            bit -= 1;
        }
        table[dividend as usize] = remainder;
    }
    table
}

fn crc8(m: &[u8]) -> u8
{
    let mut remainder: u8 = 0;

    for byte in m.iter() {
        remainder = CRC_TABLE[(remainder ^ byte) as usize];
    }
    remainder
}

#[repr(u8)]
#[derive(Debug)]
enum MessageType {
    Version=1,
    Ping=2,
    Ack=3,
    SamplerRegisterVariable=4,
    SamplerClear=5,
    SamplerStart=6,
    SamplerStop=7,
    SamplerSample=8
}

impl MessageType {
    fn from_u8(v: u8) -> Option<MessageType>
    {
        match v {
            1 => Some(MessageType::Version),
            2 => Some(MessageType::Ping),
            3 => Some(MessageType::Ack),
            4 => Some(MessageType::SamplerRegisterVariable),
            5 => Some(MessageType::SamplerClear),
            6 => Some(MessageType::SamplerStart),
            7 => Some(MessageType::SamplerStop),
            8 => Some(MessageType::SamplerSample),
            _ => None
        }
    }
}

#[derive(Debug)]
struct Message {
    ztype: MessageType,
    len: u16,
    seq: u8,
    payload_crc: u8,
    header_crc: u8,
    payload: Vec<u8>,
}

fn encode_version(reply_to_seq: u8, version: u16) -> Vec<u8>
{
    encode(MessageType::Version, 0 /* TODO */, &vec![(version & 0xff) as u8, (version >> 8) as u8, reply_to_seq, 0u8])
}

fn encode_ack(error: u16, reply_to_seq: u8) -> Vec<u8>
{
    encode(MessageType::Ack, 0 /* TODO */, &vec![(error & 0xff) as u8, (error >> 8) as u8, reply_to_seq])
}

fn encode_sampler_sample(ticks: u32, vars: &Vec<u8>) -> Vec<u8>
{
    // TODO playing with bits is nice and all.. but a rust python.struct.encode please?
    let mut payload = vec![
        (ticks & 0xff) as u8,
        ((ticks >> 8) & 0xff) as u8,
        ((ticks >> 16) & 0xff) as u8,
        (ticks >> 24) as u8
        ];
    payload.extend_from_slice(vars);
    encode(MessageType::SamplerSample, 0 /* TODO */,  &payload)
}

fn encode(ztype: MessageType, seq: u8, payload: &Vec<u8>) -> Vec<u8>
{
    let payload_crc = crc8(payload);

    // TODO - this is actually less readable than the c code, since I'm reapting
    // offsets here that are known by the compiler already (offset to crc, order
    // of fields).
    let len = payload.len();
    let mut header: Vec<u8> = vec!['E' as u8, 'M' as u8, ztype as u8, (len & 0xff) as u8, (len >> 8) as u8, seq, payload_crc, 0u8];
    header[7] = crc8(&header[..7]);
    header.extend_from_slice(payload);
    header
}

fn parse(m: &Vec<u8>) -> Result<(Message, usize), usize>
{
    if m.len() < 8 {
        return Err(0); // don't skip anything
    }
    if m[0] != 'E' as u8 {
        return Err(1); // error sig, skip
    }
    if m[1] != 'M' as u8 {
        return Err(2); // skip both
    }
    let ztype = m[2];
    let known = ztype >= 1 && ztype <= 8; // shortcut - wanted to use match
    let len = m[3] as u16 + (m[4] as u16 * 256); // little endian
    let seq = m[5];
    let payload_crc = m[6];
    let header_crc = m[7];
    let total_size = len as usize + 8;
    if m.len() < total_size {
        return Err(0);
    }
    println!("len: {}", len);
    println!("seq: {}", seq);
    println!("type: {}", ztype);
    println!("payload_crc: {}", payload_crc);
    println!("header_crc: {}", header_crc);

    let payload = m[8..].to_vec();
    let calc_payload_crc = crc8(&payload);
    let calc_header_crc = crc8(&m[..7]);
    println!("calc_payload_crc: {}", calc_payload_crc);
    println!("calc_header_crc: {}", calc_header_crc);

    if calc_header_crc != header_crc || calc_payload_crc != payload_crc {
        return Err(total_size);
    }

    if !known {
        println!("skipping unknown message (should we NACK it? what does the protocol say?)");
        return Err(total_size);
    }
    Ok((Message {
            ztype: MessageType::from_u8(ztype).unwrap(),
            seq: seq,
            len: len,
            payload_crc: payload_crc,
            header_crc: header_crc,
            payload: payload,
        }, total_size))
}

#[derive(Debug)]
struct SamplerRegisterVariable {
    phase_ticks: u32,
    period_ticks: u32,
    address: u32,
    size: u16,
}

fn little_endian_u32(v: &[u8]) -> u32
{
    v[0] as u32 + ((v[1] as u32) << 8) + ((v[2] as u32) << 16) + ((v[3] as u32) << 24)
}

fn little_endian_u16(v: &[u8]) -> u16
{
    v[0] as u16 + ((v[1] as u16) << 8)
}

impl SamplerRegisterVariable {
    fn from(payload: &Vec<u8>) -> SamplerRegisterVariable {
        SamplerRegisterVariable {
            phase_ticks: little_endian_u32(&payload[..4]),
            period_ticks: little_endian_u32(&payload[4..8]),
            address: little_endian_u32(&payload[8..12]),
            size: little_endian_u16(&payload[12..14])
        }
    }
}

// expect identical messages
struct Sampler {
    vars: Vec<SamplerRegisterVariable>,
    running: bool,
}

impl Sampler {
    fn new() -> Sampler {
        Sampler { vars: Vec::new(), running: false }
    }
}

struct State {
    sampler: Sampler,
    ticks: u32,
}

impl State {
    fn new() -> State {
        State { sampler: Sampler::new(), ticks: 0u32 }
    }

    fn act(&mut self, m: &Message) -> Vec<u8>
    {
        match m.ztype {
            MessageType::Version => encode_version(m.seq, EMOLOG_PROTOCOL_VERSION),
            MessageType::SamplerStop => {
                self.sampler.running = false;
                encode_ack(0u16, m.seq)
            },
            MessageType::SamplerClear => {
                self.sampler.vars.clear();
                encode_ack(0u16, m.seq)
            },
            MessageType::SamplerRegisterVariable => {
                let var = SamplerRegisterVariable::from(&m.payload);
                println!("got registration of {:?}", var);
                self.sampler.vars.push(var);
                encode_ack(0u16, m.seq)
            },
            MessageType::SamplerStart => {
                self.sampler.running = true;
                encode_ack(0u16, m.seq)
            },
            _ => Vec::new()
        }
    }

    fn vars(&mut self) -> Vec<u8>
    {
        self.ticks += 1;
        if ! self.sampler.running {
            return Vec::new();
        }

        let mut vars: Vec<u8> = Vec::new();
        for var in &self.sampler.vars {
            if (var.phase_ticks + self.ticks) % var.period_ticks == 0 {
                for _i in 0..var.size {
                    vars.push((self.ticks % 2) as u8);
                }
            }
        }

        if vars.len() == 0 {
            Vec::new()
        } else {
            encode_sampler_sample(self.ticks, &vars)
        }
    }
}

struct Interactor<'a, T: 'a + SerialPort> {
    port: &'a mut T,
    count: u32,
}

impl<'a, T> Interactor<'a, T> where T: 'a + SerialPort {
    fn send_vec(&mut self, send: &Vec<u8>) {
        if send.len() > 0 {
            if self.count == 0 { println!("sending {:?}", send) };
            self.port.write(&send); // TODO - allow to fail, means UART buffer is full, dropped packet, expected to happen
        }
        self.count = (self.count + 1) % 10;
    }

    fn read(&mut self, s: &mut [u8]) -> io::Result<usize> {
        self.port.read(s)
    }
}


fn interact<T: SerialPort>(port: &mut T) -> io::Result<()> {
    try!(port.reconfigure(&|settings| {
        try!(settings.set_baud_rate(serial::Baud115200));
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }));

    let mut inter = Interactor { port: port, count: 0u32 };

    let mut buf: Vec<u8> = (0..255).collect();

    let mut recv : Vec<u8> = Vec::new();

    let mut state = State::new();

    loop {
        match inter.read(&mut buf[..]) {
            Ok(num) => {
                for i in 0..num {
                    recv.push(buf[i]);
                };
                println!("got {} (total {:?})", num, recv.len())
            },
            _ => () //println!("."),
        }
        loop {
            match parse(&recv) {
                Ok((m, n)) => {
                    println!("msg {:?}", m);
                    recv = recv[n..].to_vec(); //.split_off(n);
                    let send = state.act(&m);
                    inter.send_vec(&send);
                },
                Err(skip) => {
                    recv = recv[skip..].to_vec(); //.split_off(skip);
                    break;
                },
            }
        }
        {
            let send = state.vars();
            inter.send_vec(&send);
        }
    }

    Ok(())
}

